#!/bin/bash

DATASET=( "Baby1" "Baby3" "Bowling2" "Cloth2" "Cloth4" "Lampshade2" "Midd2" "Plastic" "Rocks2" "Wood2" "Aloe" "Baby2" "Bowling1" "Cloth1" "Cloth3" "Flowerpots" "Lampshade1" "Midd1" "Monopoly" "Rocks1" "Wood1" )

unzip -n ALL-2views.zip 2>&1 1>/dev/null

for dir in ${DATASET[@]}
do
	cd $dir

	if [ ! -e "cam_lx.bmp" ] ; then
		pngtopnm "view1.png" | ppmtobmp -bpp=24 > "cam_lx.bmp"
	fi
	if [ ! -e "cam_rx.bmp" ] ; then
		pngtopnm "view5.png" | ppmtobmp -bpp=24 > "cam_rx.bmp"
	fi
	if [ ! -e "ref_lx.bmp" ] ; then
		pngtopnm "disp1.png" | ppmtobmp -bpp=8 > "ref_lx.bmp"
	fi
	if [ ! -e "ref_rx.bmp" ] ; then
		pngtopnm "disp5.png" | ppmtobmp -bpp=8 > "ref_rx.bmp"
	fi

	cd ..
done


if [ ! -e "Tsukuba/left/tsukuba_daylight_L_00001.png" ]; then
	echo -e "\n\n\n*****************************************************************"
	echo "In order to install the Tsukuba dataset, please download the"
	echo "full version at the following address ( ~5GB ):              "
	echo
	echo " http://www.cvlab.cs.tsukuba.ac.jp/dataset/tsukubastereo.php "
	echo
	echo "and copy the folders as stated next:"
	echo " illumination/daylight/*         -> dataset/Tsukuba/*"
	echo " groundtruth/disparity_maps/left -> dataset/Tsukuba/disparity_maps"
	echo -e "*****************************************************************"
else
	MEASURE=$(identify Tsukuba/left/tsukuba_daylight_L_00001.png | awk '{ print $3 }')
	if [ "$MEASURE" != "427x320" ]; then
		sh converter.sh
	fi
fi



exit 0
