#!/bin/bash


################################################################################
# Demo Confiugration
################################################################################

BOSP_BASE="${BOSP_BASE:-$HOME/BOSP}"
DEMO_BASE="${DEMO_BASE:-${BOSP_BASE}/contrib/user/mview-demo-argo}"
MVIEW_PLOT="${MVIEW_PLOT:-$DEMO_BASE/scripts}"
MVIEW_CONF="${MVIEW_CONF:-$DEMO_BASE/conf}"

BBQUE_SYSROOT="${BBQUE_SYSROOT:-$BOSP_BASE/out}"
BBQUE_LOGFILE="${BBQUE_LOGFILE:-$BBQUE_SYSROOT/var/bbque.log}"

MVIEW_EXEC="${MVIEW_EXEC:-${BBQUE_SYSROOT}/usr/bin/mview-demo-argo}"
MVIEW_START_DELAY="${MVIEW_START_DELAY:-1}"
VIDEO_PATH="${VIDEO_PATH:-${DEMO_BASE}/dataset/Tsukuba}"
VIDEO_GRAYSCALE="${VIDEO_GRAYSCALE:-3}"
VIDEO_FPS="${VIDEO_FPS:-5}"
STRESS_TEST_NUMBER=5
if [ $# -eq 1 ]
  then
    STRESS_TEST_NUMBER=$1
fi


################################################################################
# MultiView Specific Configuration
################################################################################


echo "Deploy configuration files..."
cp -v $MVIEW_CONF/bbque.conf            $BBQUE_SYSROOT/etc/bbque/bbque.conf
cp -v $MVIEW_CONF/demo_pil.bpl          $BBQUE_SYSROOT/etc/bbque/pil/${HOSTNAME}.bpl

echo "Deploy recipes..."
cp -v $MVIEW_CONF/*.recipe              $BBQUE_SYSROOT/etc/bbque/recipes/

echo "Cleanup previous run..."
rm -f $BBQUE_LOGFILE
rm -f $MVIEW_LOGFILE

echo "Setup BOSP shell..."
source $BBQUE_SYSROOT/etc/bbque/bosp_init.env
bbque-stopd &>/dev/null



################################################################################
# Do not tuch under this marker
################################################################################

MVIEW=./out_runMview.sh
cat > $MVIEW <<EOF
#!/bin/bash -x

MVID=\$1
DATASET_NAME=\$2
VIDEO_GRAYSCALE=\$3
GOAL_FPS=\$5

# Run MultiView plotter
# \$1 - Mview logfile
function runPlotter() {
        LOGFILE=\$1

        PLOTDIR=\`dirname \$LOGFILE\`
        mkdir \$PLOTDIR &>/dev/null
        rm \$PLOTDIR/* &>/dev/null

        # Setup plot scripts
        cd \$PLOTDIR
        ln -s $MVIEW_PLOT/display_data.py
        chmod +x display_data.py

        # Start the plotter
        ./display_data.py --log_file \$1 --instance_id \$MVID &
}


# Run MultiView
function runMview() {
        # Keep track of current Mview instance, this is required
        # in order to instantiate different plotters
        MVIEW_LOGFILE=$MVIEW_PLOT/mview_\$MVID/mview.log

        runPlotter \$MVIEW_LOGFILE
        sleep 1

        # Start a new multiview instance
	$MVIEW_EXEC \\
		--instance \${MVID} \\
		--datapath \${DATASET_NAME} \\
		--grayscale \${VIDEO_GRAYSCALE} \\
		--fps \${GOAL_FPS} \\
                --log_file \$MVIEW_LOGFILE


        sleep 20
}

runMview

EOF
chmod a+x $MVIEW


# DEMO CONFIGURATION
# - XWindows geometries recoveder using "xwininfo" utility
# - Font is configured by "-fn" option, to see a list of fonts run:
#     "xlsfonts | less"
FONT=-misc-fixed-medium-r-normal--10-100-75-75-c-60-iso10646-1


print_splash() {
clear

echo -ne "\n\n\E[1m\E[30;42m"
echo "                                                                          "
echo "               MultiView + BarbequeRTRM + Argo -  onRun() Demo            "
echo "                         https://bitbucket.org/bosp/                      "
echo -ne "\E[0m\E[30;42m"
echo "                                                                          "
echo "                    HARPA Project - FP7-ICT-2013-10-612069                "
echo "                   Harnessing Performance Variability                     "
echo "                         http://www.harpa-project.eu                      "
echo "                                                                          "
echo -ne "$BOSPSH_RESET$BOSPSH_DEFAULT"
}

print_title() {
print_splash
echo -e "\n\n\E[1m.:: $1\E[0m\n"
}

press_to_continue() {
echo
read -p "Press Return key to continue..."
}

ask_action() {
echo
read -p "$1, press Return key to continue"
}


#################################################################################
##     BBQUE Console
#################################################################################
#
#print_title "BBQ Console startup"
#
#echo "The BarbequeRTRM framework comes with \"management shell\" which provides not"
#echo "only a set of simplified command to interact with the framework but also a"
#echo "\"monitoring console\" to easily verify resources assignement."
#
#echo "Press a KEY to start the BBQ Console"
#read KEY
#
#bbque-console
#
#echo "The BBQ console consists of 4 main windows, which represent, starting from the"
#echo "top left corner and rotating clockwise:"
#echo "1. a system logger viewer, where daemon startup is reported"
#echo "2. a \"Resources Monitor\" viewer, where availability of HOST and MANAGED"
#echo "   resources is monitored"
#echo "3. a \"Task Monitor\" viewer, which show a list of managed applications and the"
#echo "   resources being assigned to them"
#echo "4. a \"CPU Usages\" viewer, which show the actual usage of available processor"
#
#echo "Press a KEY to continue..."
#read KEY

################################################################################
#     BBQUE Daemon Startup
################################################################################

print_title "System Initialization"

echo "The multi-core machine is initially configured, by the BarbequeRTRM"
echo "deamon startup, to reserve a set of resources for run-time managed applications."
echo ""
echo "The 16-core (16 HT) machine is partitioned into:"
echo "* 4  CPU cores: [HOST device], where all generic tasks are moved and will run"
echo "* 12 CPU cores: [MANAGED device], where BBQ managed applications will be"
echo "                deploied a set of workloads with different priorities are"
echo "                started concurrently."
echo ""
echo "NOTE: Starting the daemon requires root priviledges: enter the passowrd when required"
#################################################################################
press_to_continue


bbque-startd $MVIEW_CONF/bbque.conf
bbque-log $BBQUE_LOGFILE
bbque-console

echo "Now you should be able to check the set of managed resources looking at the"
echo "\"Resources Monitor\"."
echo "Moreover, by looking a <the \"CPU Usages\" you should notice that all actively"
echo "running applications have been moved into the HOST device."
#################################################################################
press_to_continue


################################################################################
#    Demo description
################################################################################

print_title "Demo - Office exploration"


echo "In this scenario, we are a flying robot that explore an office."
echo "The robot has a pair of stereo-camera that capture the images toward the"
echo "front side. These images are processed by a Multiview OpenMP application,"
echo "which applies image stereo-matching to compute the pixel-disparity. The"
echo "result is used to detect any potential object that can collide with the"
echo "robot, if it keeps going in its direction."
echo "Platform resources (the CPU quota) are assigned by the BarbequeRTRM to the"
echo "Multiview application according to its runtime requirements. Beside that,"
echo "at the application level the AS-RTM library allows for runtime monitoring "
echo "and adaptivity, selecting the operating point that best fit the runtime"
echo "requirements."
echo "The application can define a set of constraints on any metric or parameter"
echo "that is contained in the operating operating, and can define also a rank"
echo "function that assign to every op a value that define their quality from"
echo "the application point of view. Additionally the AS-RTM enables to define"
echo "sevaral application states."
echo "In this scenario the application uses two states:"
echo "   Normal -> If there is no object close to the robot"
echo "   Danger -> If there is an object near the robot"
echo "In the Normal state the application has a contraint on the error that has"
echo "higher priority over the constraint on the fps. In the Danger state the"
echo "fps constraint is the one with maximum priority."

#################################################################################
press_to_continue

ID=1
WIN_NAME="Camera-$ID"
aterm +sb -fn $FONT -geometry 105x50+0+0 -title ${WIN_NAME} \
		-e $MVIEW $ID $VIDEO_PATH $VIDEO_GRAYSCALE $WIN_NAME $VIDEO_FPS &> /dev/null &


sleep $MVIEW_START_DELAY

echo "Check the output video, now there is an istance of the application that runs"
echo "without generating resource contention. The next step launch random application"
echo "in order to disturb the application and observe the application adapt"
#################################################################################
press_to_continue


${MVIEW_PLOT}/mview-demo-argo-stress ${STRESS_TEST_NUMBER} ${BOSP_BASE} &


echo "Check the output video, as you can see when the application state is Normal"
echo "the mview instance give priority to the thorughput. When the application"
echo "state is Danger, the throughput get the priority"



press_to_continue
killall bbque-testapp

################################################################################
#    Demo wrap-up
################################################################################

bbque-console-stop

killall mview-demo-argo aterm \
        python \
	bbque-testapp \
	mview-demo-argo-stress \
	sleep \
        &>/dev/null
rm -rf plot/mview_*
rm -f $MVIEW

bbque-stopd
bosp-cleanup

exit 0
#!/bin/bash
