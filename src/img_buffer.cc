/**
 *       @file  img_buffer.cc
 *      @brief  The OpenMP-MView-ARGO BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *              Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <exception>
#include <iostream>

#include <img_buffer.h>


// utility function for padding a number to fixed lenght
# define TSUKUBA_DATASET_PADDING_NUMBER 5
static inline std::string get_padded_suffix( const int frame_number, const int num_padding ) {
	
	//const int num_padding = 5;
	
	std::string result = std::to_string(frame_number);
	
	const unsigned int limit = (num_padding - result.size());
	
	for(unsigned int i = 0; i < limit; i++) {
		result = "0" + result;
	}
	
	return result;
}


std::string ImgBuffer::get_left_image_name(void)
{
	std::string file_name = base_file_name;
	
	if ( dataset_name.compare("Tsukuba") == 0 )
	{
		// get the suffix
		file_name += "left/tsukuba_daylight_L_" + get_padded_suffix(frame_counter, TSUKUBA_DATASET_PADDING_NUMBER) + ".png";
	} else
	{
		file_name += "cam_lx.bmp";
	}
	
	return file_name;
}


std::string ImgBuffer::get_right_image_name(void)
{
	std::string file_name = base_file_name;
	
	if ( dataset_name.compare("Tsukuba") == 0 )
	{
		// get the suffix
		file_name += "right/tsukuba_daylight_R_" + get_padded_suffix(frame_counter, TSUKUBA_DATASET_PADDING_NUMBER) + ".png";
	} else
	{
		file_name += "cam_rx.bmp";
	}
	
	return file_name;
}



ImgBuffer::ImgBuffer(
	std::string dataset, 
	const unsigned int proximity_box_size,
	unsigned int starting_frame):
	square_size(proximity_box_size)
{
	/***********************************
	 * Initialize the dataset parameters
	 ***********************************/
	
	
	// var intialization
	std::string delimiter = "/";
	base_file_name = "";
	frame_counter = 1;
	
	// tokenize the path
	size_t pos = 0;
	while ((pos = dataset.find(delimiter)) != std::string::npos) {
		dataset_name = dataset.substr(0, pos);
		base_file_name += dataset_name + "/";
		dataset.erase(0, pos + delimiter.length());
	}
	
	// complete the var
	dataset_name = dataset;
	base_file_name += dataset_name + "/";
	
	
	// start dataset specific initialization
	if ( dataset_name.compare("Tsukuba") == 0 ) {
		frame_number = 1800;
	} else {
		frame_number = 1;
	}
	
	// save the reference image
	reference_img_name = base_file_name;
	if ( dataset_name.compare("Tsukuba") == 0 )
	{
		// get the suffix
		reference_img_name += "right/tsukuba_daylight_R_" + get_padded_suffix(frame_counter, TSUKUBA_DATASET_PADDING_NUMBER) + ".png";
	} else
	{
		reference_img_name += "cam_rx.bmp";
	}
	
	
	/***********************************
	 * Load the first images
	 ***********************************/
	
	// load the left image
	left = cv::imread(get_left_image_name(), CV_LOAD_IMAGE_COLOR);
	if (!left.data) {
		throw std::runtime_error("Failed to load the left image");
	}
	
	// load the left image
	right = cv::imread(get_right_image_name(), CV_LOAD_IMAGE_COLOR);
	if (!left.data) {
		throw std::runtime_error("Failed to load the right image");
	}
	
	// get the image stat
	width = left.size().width;
	height = left.size().height;
	type = left.type();
	
	// check if the images are with the same size
	if ( right.size().width != width || right.size().height != height) {
		throw std::runtime_error("The two images don't have the same size");
	}
	
	
	/***********************************
	 * Dimension the proximity regions
	 ***********************************/
	
	// dimension the array
	col_n = width/square_size;
	row_n = height/square_size;
	
	// initialize it
	regions.resize(row_n);
	for (unsigned int i = 0; i < row_n; i++)
	{
		regions[i].resize(col_n, 0);
	}
	
	
	/***********************************
	 * Initialize the other vars
	 ***********************************/
	out = cv::Mat(height,width,type, cv::Scalar::all(0));
	state = ApplicationState::Normal;
	status = ExecutionStatus::Running;
}

void ImgBuffer::getFirstReferenceFrame(cv::Mat& reference)
{
	// load the image
	reference = cv::imread(reference_img_name, CV_LOAD_IMAGE_GRAYSCALE);
	if (!reference.data) {
		throw std::runtime_error("Failed to load the reference image");
	}
	
	// check if the images are with the same size
	if ( reference.size().width != width || reference.size().height != height) {
		throw std::runtime_error("The reference image has a wrong size");
	}

}

void ImgBuffer::getImageStat(int& width, int& height, int& type)
{
	width = this->width;
	height = this->height;
	type = this->type;

}


void ImgBuffer::getInputImages(cv::Mat& left, cv::Mat& right)
{
	// lock the buffer
	std::lock_guard<std::mutex> lg(buffer_mutex);
	
	// copy the images
	this->left.copyTo(left);
	this->right.copyTo(right);
}

void ImgBuffer::getOutput(ExecutionStatus& status, ApplicationState& state, proxRegions& proximity, cv::Mat& output)
{
	// lock the buffer
	std::lock_guard<std::mutex> lg(buffer_mutex);
	
	// copy the stats
	status = this->status;
	state = this->state;
	proximity = this->regions;
	this->out.copyTo(output);
}

void ImgBuffer::getProximityRegionStat(unsigned int& square_size, unsigned int& row_number, unsigned int& col_number)
{
	square_size = this->square_size;
	row_number = row_n;
	col_number = col_n;
}

void ImgBuffer::next()
{
	// lock the buffer
	std::lock_guard<std::mutex> lg(buffer_mutex);
	
	// increment the counter
	if ( frame_number == frame_counter )
	{
		// restart from the first frame
		frame_counter = 1;
	} else
	{
		// keep going
		frame_counter++;
	}
	
	// load the left image
	left.release();
	left = cv::imread(get_left_image_name(), CV_LOAD_IMAGE_COLOR);
	if (!left.data) {
		throw std::runtime_error("Failed to load the left image");
	}
	
	// load the left image
	right.release();
	right = cv::imread(get_right_image_name(), CV_LOAD_IMAGE_COLOR);
	if (!left.data) {
		throw std::runtime_error("Failed to load the right image");
	}
	
	// check the sizes
	if ( right.size().width != width || right.size().height != height) {
		throw std::runtime_error("The right image has a wrong size");
	}
	if ( left.size().width != width || left.size().height != height) {
		throw std::runtime_error("The left image has a wrong size");
	}
	

}

void ImgBuffer::setOutput(const ExecutionStatus status, const ApplicationState state, const proxRegions proximity, const cv::Mat& output)
{
	// lock the buffer
	std::lock_guard<std::mutex> lg(buffer_mutex);
	
	// copy the state
	this->state = state;
	this->status = status;
	this->regions = proximity;
	
	//TODO verificare che non si possa fare in modo più efficiente
	cv::Mat converted_disp;
	cv::cvtColor(output, converted_disp, CV_GRAY2BGR);
	converted_disp.copyTo(out);
}
